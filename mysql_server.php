<?php

class Post {
    public int $id;
	public string $title;
	public string $description;
	public string $content;
	public int $author_id;
}

try{
    $host = "localhost:3306";
    $dbname = "alatoo";
    $username = "root";
    $password = "erke123";

    $conn = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $conn->prepare("SELECT * FROM posts");
    $stmt->execute();
    $posts = $stmt->fetchAll(PDO::FETCH_CLASS, "Post");

    echo $posts[0]->title;
}catch(PDOException $error){
    echo $error;
}