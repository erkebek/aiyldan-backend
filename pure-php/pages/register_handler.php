<?php

$db = db();
// var_dump($_POST);die;


$name = $_POST['name'];
$login = $_POST['login'];
$password = $_POST['password'];
$password_confirmation = $_POST['password_confirmation'];

$error_message = "";

if($password != $password_confirmation){
    $error_message = "Сырсөз такталган жок";
}

foreach($db->getUsers() as $currentUser){
    if($currentUser->login == $login){
        $error_message = "Мындай колдонуучу катталган";
    }
}

if($error_message != ""){
    header("Location: /register?message=" . $error_message);
}else{
    $user = new User($login, $password, $name);
    $db->saveUser($user);
    $_SESSION['user'] = $user;
    header("Location: /");
}