<?php

$db = db();
// var_dump($_POST);

$login = $_POST['login'];
$password = $_POST['password'];

$user = null;

// for($i = 0; $i < sizeof($db->getUsers()); $i++){
//     $user = $db->getUsers()[$i];
// }

foreach($db->getUsers() as $currentUser){
    if($currentUser->login == $login && $currentUser->password == $password){
        $user = $currentUser;
    }
}

if($user != null){
    $_SESSION['user'] = $user;
    header("Location: /");
}else{
    header("Location: /login?message=User not found");
}