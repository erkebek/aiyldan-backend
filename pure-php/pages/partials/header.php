<nav class="navbar navbar-expand-lg navbar-dark bg-dark px-lg-5 p-lg-0">
    <a class="navbar-brand" href="/">Балуу кеңештер</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/"><i class="fa fa-question-circle"></i> Башкы бет
                </a>
            </li>
            <?php if($authorizedUser == null): ?>
            <li class="nav-item">
                <a class="nav-link" href="/register"><i class="fa fa-sign-in"></i> Катталуу</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/login"><i class="fa fa-sign-in"></i> Кирүү</a>
            </li>
            <?php else: ?>
            <li class="nav-item">
                <a class="nav-link"><i class="fa fa-user"></i> Кутмандуу күнүңүз менен <?= $authorizedUser->name ?></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/logout"><i class="fa fa-sign-in"></i> Чыгуу</a>
            </li>
            <?php endif; ?>
        </ul>
    </div>
</nav>