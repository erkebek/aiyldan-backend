<?php

function printv($var){
    echo $var . "\n";
}

class Person{
    public string $name;
    
    static public int $count = 0;

    public function __construct($name){
        $this->name = $name;
        self::$count++;
    }

}

class Sun{
    private static ?Sun $instance = null;
    public string $name;
    private function __construct(){}

    public static function getInstance(){
        if(self::$instance == null){
            self::$instance = new Sun();
        }
        return self::$instance;
    }
}

$person1 = new Person("Aibek");
printv(Person::$count);
$person2 = new Person("Aiperi");

$person1->name = "Adilet";

printv($person1->name);
printv($person2->name);

new Person("Name");

printv(Person::$count);

$sun = Sun::getInstance();

$sun->name = "Sun 1";

printv($sun->name);
$sun->name = "Sun bashka";

$sun2 = Sun::getInstance();

printv($sun2->name);
printv($sun->name);